
/**
 *
 *         _______       ___       ____      __       _______
 *        /\  ____\    /|   \     /  __\    /\ \     /\  ____\
 *        \ \ \___/_   ||  _ \   |  /__/____\ \ \    \ \ \___/_
 *         \ \  ____\  || |_\ \  |\ \ /\_  _\\ \ \    \ \  ____\
 *          \ \ \___/_ ||  ___ \ \ \ \\//\ \/ \ \ \____\ \ \___/_
 *           \ \______\||_|__/\_\ \ \ \_\/ |   \ \_____\\ \______\
 *            \/______/|/_/  \/_/  \_\_____/    \/_____/ \/______/
 *
 *
 *    Eagle Agile Gui Library and Extensions
 *
 *    Copyright 2009-2021+ by Edgar Reynaldo
 *
 *    See EagleLicense.txt for allowed uses of this library.
 *
 * @file Allegro5Resources.hpp
 * @brief Allegro 5 resource classes
 */

#ifndef Allegro5Resources_HPP
#define Allegro5Resources_HPP



#include "Eagle/Resources.hpp"



class Allegro5ImageResource : public ImageResource {
public :
};



class Allegro5FontResource : public FontResource {
public :
};



class Allegro5AudioResource : public AudioResource {
public :
};



class Allegro5VideoResource : public VideoResource {
public :
};



class Allegro5ArchiveResource : public ArchiveResource {
public :
};



class Allegro5BinaryResource : public BinaryResource {
public :
};



class Allegro5TextResource : public ResourceBase {
public :
};



#endif // Allegro5Resources_HPP

